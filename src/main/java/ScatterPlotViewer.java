package main.java;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import main.java.models.Order;
import main.java.models.Shop;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class ScatterPlotViewer extends JFrame {

    public final String imagePath = "/src/main/resources/map.jpg";
    private XYPlot plot;


    public ScatterPlotViewer(ArrayList<Shop> shopList) {
        super("Mandalina");

        XYDataset dataSet = createDataset(shopList);
        JFreeChart chart = ChartFactory.createScatterPlot(
                "Sipariş - Bayi Grafiği",
                "Longitude", "Latitude", dataSet);


        try {
            File pathToFile = new File(System.getProperty("user.dir")+ imagePath);
            Image image = ImageIO.read(pathToFile);

            plot = (XYPlot) chart.getPlot();
            plot.setDomainGridlinesVisible(false);
            plot.setRangeGridlinesVisible(false);
            plot.setBackgroundImageAlpha(1);
            plot.setBackgroundImage(image);

            setStyleForDataset(shopList);

            ChartPanel panel = new ChartPanel(chart);
            setContentPane(panel);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private XYDataset createDataset(ArrayList<Shop> shopList) {
        XYSeriesCollection dataSet = new XYSeriesCollection();

        for (Shop s : shopList) {
            XYSeries series = new XYSeries(s.getShopName());
            for (Order o : s.getAssignedOrders()) {
                series.add(o.getLongitude(), o.getLatitude());
            }
            dataSet.addSeries(series);

            series = new XYSeries("Bayi: "+s.getShopName());
            series.add(s.getLongitude(), s.getLatitude());
            dataSet.addSeries(series);
        }

        return dataSet;
    }


    private void setStyleForDataset(ArrayList<Shop> shopList){

        for(int i=0; i<shopList.size()*2; i++){
            Shop s = shopList.get(i/2);

            if(i%2 == 0){
                plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(i, s.getShopColor());
                plot.getRendererForDataset(plot.getDataset(0)).setSeriesShape(i, new Ellipse2D.Double(-3, -3, 6, 6));
            }else{
                plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(i, s.getShopColor());
                plot.getRendererForDataset(plot.getDataset(0)).setSeriesShape(i, ShapeUtilities.createDiamond(8));
            }
        }
    }
}
