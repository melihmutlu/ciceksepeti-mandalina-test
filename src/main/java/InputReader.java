package main.java;

import main.java.helpers.ExcelHelper;
import main.java.models.Order;
import main.java.models.Shop;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public  class InputReader {

    private static ExcelHelper helper;

    public static void prepareInput(String fileName, ArrayList<Order> orderList, ArrayList<Shop> shopList){
        readInput(fileName, orderList, shopList);
        setQuotas(shopList);
        setColors(shopList);
    }

    private static void readInput(String fileName, ArrayList<Order> orderList, ArrayList<Shop> shopList){
        try {
            helper = new ExcelHelper(fileName);
            orderList.addAll(helper.loadOrders());
            shopList.addAll(helper.loadShops());
            helper.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void setQuotas(ArrayList<Shop> shopList){
        HashMap<String, Integer[]> quotas = new HashMap<>();
        quotas.put("Kırmızı ", new Integer[]{20, 30});
        quotas.put("Mavi", new Integer[]{20, 80});
        quotas.put("Yeşil", new Integer[]{35, 50});

        for(Shop s : shopList){
            Integer[] shopQuota = quotas.get(s.getShopName());
            s.setMinQuota(shopQuota[0]);
            s.setMaxQuota(shopQuota[1]);}
    }

    private static void setColors(ArrayList<Shop> shopList){
        for(Shop s : shopList){
            switch (s.getShopName()){
                case "Kırmızı ": s.setShopColor(Color.RED); break;
                case "Mavi" : s.setShopColor(Color.BLUE); break;
                case "Yeşil" : s.setShopColor(Color.GREEN); break;
                default: break;
            }
        }
    }
}
