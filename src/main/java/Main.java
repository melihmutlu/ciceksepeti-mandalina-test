package main.java;

import main.java.models.Order;
import main.java.models.Shop;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

    private static final String inputFile = "input/siparis ve bayi koordinatları.xlsx";

    private static ArrayList<Order> orderList = new ArrayList<>();
    private static ArrayList<Shop> shopList = new ArrayList<>();
    private static OrderManager manager;

    public static void main(String[] args) {

        InputReader.prepareInput(inputFile, orderList, shopList);

        manager = new OrderManager(orderList, shopList);
        manager.run();

        SwingUtilities.invokeLater(() -> {
            ScatterPlotViewer viewer = new ScatterPlotViewer(shopList);
            viewer.setSize(722, 848);
            viewer.setLocationRelativeTo(null);
            viewer.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            viewer.setVisible(true);
        });
    }
}
