package main.java.models;

import java.awt.*;
import java.util.ArrayList;

public class Shop {
    private int shopId;
    private String shopName;
    private double latitude;
    private double longitude;
    private int minQuota;
    private int maxQuota;
    private Color shopColor;
    private ArrayList<Order> assignedOrders;

    public Shop(int shopId, String shopName, double latitude, double longitude, int minQuota, int maxQuota) {
        this.shopId = shopId;
        this.shopName = shopName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.minQuota = minQuota;
        this.maxQuota = maxQuota;
        assignedOrders = new ArrayList<>();
    }

    public Shop(int shopId, String shopName, double latitude, double longitude) {
        this.shopId = shopId;
        this.shopName = shopName;
        this.latitude = latitude;
        this.longitude = longitude;
        assignedOrders = new ArrayList<>();
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getMinQuota() {
        return minQuota;
    }

    public void setMinQuota(int minQuota) {
        this.minQuota = minQuota;
    }

    public int getMaxQuota() {
        return maxQuota;
    }

    public void setMaxQuota(int maxQuota) {
        this.maxQuota = maxQuota;
    }

    public ArrayList<Order> getAssignedOrders() {
        return assignedOrders;
    }

    public Color getShopColor() {
        return shopColor;
    }

    public void setShopColor(Color shopColor) {
        this.shopColor = shopColor;
    }


    public int addOrder(Order o){
        if(assignedOrders.size() >= maxQuota){
            return -1;
        }
        assignedOrders.add(o);
        return 0;
    }

    public boolean hasQuota(){
        return assignedOrders.size() < maxQuota;
    }

}
