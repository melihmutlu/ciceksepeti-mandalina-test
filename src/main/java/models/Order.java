package main.java.models;

import java.util.ArrayList;

public class Order implements Comparable<Order> {
    private int orderId;
    private double latitude;
    private double longitude;
    private ArrayList<Shop> shops;
    private double minDistance = Double.MAX_VALUE;
    private double maxDistance = Double.MIN_VALUE;

    public Order(int orderId, double latitude, double longitude) {
        this.orderId = orderId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setShops(ArrayList<Shop> shops) {
        this.shops = shops;
    }


    @Override
    public int compareTo(Order o) {
        double sourceDiff = this.getLongestDistanceToShops()-this.getShortestDistanceToShops();
        double targetDiff = o.getLongestDistanceToShops() - o.getShortestDistanceToShops();
        return Double.compare(sourceDiff, targetDiff);
    }

    public double getLongestDistanceToShops(){
        for(Shop s : shops){
            double distance = Math.sqrt((this.latitude-s.getLatitude())*(this.latitude-s.getLatitude()) + (this.longitude-s.getLongitude())*(this.longitude-s.getLongitude()));
            maxDistance = distance > maxDistance ? distance : maxDistance;
        }

        return maxDistance;
    }

    public double getShortestDistanceToShops(){
        for(Shop s : shops){
            double distance = Math.sqrt((this.latitude-s.getLatitude())*(this.latitude-s.getLatitude()) + (this.longitude-s.getLongitude())*(this.longitude-s.getLongitude()));
            minDistance = distance < minDistance ? distance : minDistance;
        }

        return minDistance;
    }

    public double distanceToShop(Shop s){
        return Math.sqrt((this.latitude-s.getLatitude())*(this.latitude-s.getLatitude()) + (this.longitude-s.getLongitude())*(this.longitude-s.getLongitude()));
    }
}
