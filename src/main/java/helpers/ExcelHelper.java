package main.java.helpers;


import main.java.models.Order;
import main.java.models.Shop;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelHelper {
    private XSSFWorkbook workbook;
    private String fileName;
    private File file;
    private FileInputStream fileInputStream;

    public ExcelHelper(String fileName) throws IOException {
        this.fileName = fileName;
        this.file = new File(fileName);
        this.fileInputStream = new FileInputStream(file);
        this.workbook = new XSSFWorkbook(fileInputStream);
    }

    public ArrayList<Order> loadOrders(){
        ArrayList<Order> orderList = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIt = sheet.iterator();

        //Skip headline
        rowIt.next();

        while(rowIt.hasNext()) {
            Row row = rowIt.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                int orderId = Integer.parseInt(cellIterator.next().toString());
                double latitude = Double.parseDouble(cellIterator.next().toString());
                double longitude= Double.parseDouble(cellIterator.next().toString());

                orderList.add(new Order(orderId, latitude, longitude));
            }
        }
        return orderList;
    }

    public ArrayList<Shop> loadShops(){
        ArrayList<Shop> shopList = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheetAt(1);
        Iterator<Row> rowIt = sheet.iterator();

        //Skip headline
        rowIt.next();

        int id=0;
        while(rowIt.hasNext()) {
            Row row = rowIt.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                String shopName = cellIterator.next().toString();
                double latitude = Double.parseDouble(cellIterator.next().toString());
                double longitude= Double.parseDouble(cellIterator.next().toString());

                shopList.add(new Shop(id, shopName, latitude, longitude));
            }
            id++;
        }
        return shopList;
    }

    public void close() throws IOException{
        workbook.close();
        fileInputStream.close();
    }
}
