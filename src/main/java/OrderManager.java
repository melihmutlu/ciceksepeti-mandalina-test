package main.java;

import main.java.models.Order;
import main.java.models.Shop;

import java.util.ArrayList;
import java.util.Collections;

public class OrderManager {
    private ArrayList<Order> orderList;
    private ArrayList<Shop> shopList;

    public OrderManager(ArrayList<Order> orderList, ArrayList<Shop> shopList) {
        this.orderList = orderList;
        this.shopList = shopList;
    }

    public void run(){
        setShopListsInOrders();
        Collections.sort(orderList);
        Collections.reverse(orderList);

        assignOrders();
    }

    public void assignOrders(){
        double totalCost = 0;
        for(Order o : orderList){
            double minCost = Double.MAX_VALUE;
            Shop shopToAssign = null;
            for(Shop s : shopList){
                if(s.hasQuota()){
                    double distance = o.distanceToShop(s);
                    if(distance < minCost){
                        minCost = distance;
                        shopToAssign = s;
                    }
                }
            }
            shopToAssign.addOrder(o);
            totalCost += minCost;
        }

        System.out.println("Total Cost: " + totalCost);
    }

    public void setShopListsInOrders(){
        for(Order o : orderList){
            o.setShops(shopList);
        }
    }
}
